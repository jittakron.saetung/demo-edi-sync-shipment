﻿using Dapper;
using KerryLibDataAccess.DataAccess.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibDataAccess.DataAccess
{
    public class SqlDataAccess : ISqlDataAccess
    {
        private readonly IConfiguration _configuration;
        public SqlDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IEnumerable<T>> LoadData<T, U>(string sqlCommand, U parameters, string connectionId = "Default")
        {
            using IDbConnection connection = new SqlConnection(_configuration.GetConnectionString(connectionId));
            return await connection.QueryAsync<T>(sqlCommand, parameters,
                commandType: null);
        }

        public async Task SaveData<T>(string sqlCommand, T parameters, string connectionId = "Default")
        {
            using IDbConnection connection = new SqlConnection(_configuration.GetConnectionString(connectionId));
            await connection.ExecuteAsync(sqlCommand, parameters, commandType: null);
        }

        public async Task<T> SaveDataExecuteAsync<T, U>(string sqlCommand, U parameters, string connectionId = "Default")
        {
            using IDbConnection connection = new SqlConnection(_configuration.GetConnectionString(connectionId));
            var result = await connection.ExecuteAsync(sqlCommand, parameters, commandType: null);

            return (T)Convert.ChangeType(result, typeof(T));
        }

        public async Task<IEnumerable<T>> ExecuteStoredProcedure<T, U>(string storedProcedure, U parameters, string connectionId = "Default")
        {
            using IDbConnection connection = new SqlConnection(_configuration.GetConnectionString(connectionId));
            return await connection.QueryAsync<T>(storedProcedure, parameters,
                commandType: CommandType.StoredProcedure);
        }
        public async Task<T> ExecuteStoredProcedureOutputParams<T>(string storedProcedure, string jsonInput, string connectionId = "Default")
        {
            var parameters = new DynamicParameters();
            parameters.Add("@JsonInput", dbType: DbType.String, value: jsonInput, direction: ParameterDirection.Input);
            parameters.Add("@JsonOutput", dbType: DbType.String, direction: ParameterDirection.Output, size: int.MaxValue);
            using IDbConnection connection = new SqlConnection(_configuration.GetConnectionString(connectionId));
            var result = await connection.QueryAsync<dynamic>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
            string storedResponse = parameters.Get<string>("@JsonOutput");
            return JsonConvert.DeserializeObject<T>(storedResponse);
        }
        public async Task<IEnumerable<T>> LoadDataWithConnectionId<T, U>(string sqlCommand, U parameters, string connectionId)
        {
            using IDbConnection connection = new SqlConnection(connectionId);
            return await connection.QueryAsync<T>(sqlCommand, parameters,
                commandType: null);
        }
    }
}
