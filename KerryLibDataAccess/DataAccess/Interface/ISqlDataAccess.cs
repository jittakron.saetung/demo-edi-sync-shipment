﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibDataAccess.DataAccess.Interface
{
    public interface ISqlDataAccess
    {
        Task<IEnumerable<T>> LoadData<T, U>(string sqlCommand, U parameters, string connectionId = "Default");
        Task SaveData<T>(string sqlCommand, T parameters, string connectionId = "Default");
        Task<T> SaveDataExecuteAsync<T, U>(string sqlCommand, U parameters, string connectionId = "Default");
        Task<IEnumerable<T>> ExecuteStoredProcedure<T, U>(string storedProcedure, U parameters, string connectionId = "Default");
        Task<T> ExecuteStoredProcedureOutputParams<T>(string storedProcedure, string jsonInput, string connectionId = "Default");
        Task<IEnumerable<T>> LoadDataWithConnectionId<T, U>(string sqlCommand, U parameters, string connectionId);
    }
}
