﻿using KerryLibServices.Services.ApiServices.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace KerryLibServices.Services.ApiServices
{
    public class ApiService : IApiService
    {
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;

        public ApiService(IConfiguration configuration, HttpClient httpClient)
        {
            this._configuration = configuration;
            this._httpClient = httpClient;
        }

        public async Task<(int statusCode, string responseMessage)> RequestMethodPost(string configUrl, string methodName,string jsonRequest)
        {
            string requestRef = string.Empty;
            StringContent dataContent = new(jsonRequest, Encoding.UTF8, Application.Json);

            try
            {
                string urlEndpoint = _configuration.GetSection("APIConfigs").GetChildren().FirstOrDefault(config => config.Key == configUrl).Value;
                using var task = await _httpClient.PostAsync(Path.Combine(urlEndpoint + methodName, requestRef), dataContent);

                int statusCode = (int)task.StatusCode;
                string resultMessage = await task.Content.ReadAsStringAsync();
                return (statusCode, resultMessage);
            }
            catch (Exception)
            {
                return (500, "Internal Error");
            }
        }
    }
}
