﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Services.ApiServices.Interface
{
    public interface IApiService
    {
        Task<(int statusCode, string responseMessage)> RequestMethodPost(string configUrl, string methodName, string jsonRequest);
    }
}
