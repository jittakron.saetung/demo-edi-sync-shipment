﻿using KerryLibServices.Models.Shipment;
using KerryLibServices.Repository.Interface;
using KerryLibServices.Services.ApiServices.Interface;
using KerryLibServices.Services.ShipmentServices.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Services.ShipmentServices
{
    public class SyncShipmentInfoService : ISyncShipmentInfoService
    {
        private readonly ISyncShipmentInfoRepository _syncShipmentInfoRepository;
        private readonly IApiService _apiService;
        public SyncShipmentInfoService(ISyncShipmentInfoRepository syncShipmentInfoRepository, IApiService apiService)
        {
            this._syncShipmentInfoRepository = syncShipmentInfoRepository;
            this._apiService = apiService;
        }

        public async Task SyncShipmentInfo(int partition, int mod)
        {
            List<EdiShipmentInfo> ediShipmentInfo = await this._syncShipmentInfoRepository.GetEdiShipmentInfoSyncPending(partition, mod);
            if (ediShipmentInfo.Count > 0)
            {
                List<TmpSyncShipmentInfo> tmpSyncShipmentInfos = new();

                foreach (var item in ediShipmentInfo)
                {
                    //string endpoint = partition >= 10 ? "SyncShipmentEndpoint2" : "SyncShipmentEndpoint1";
                    //Console.WriteLine($"p: {partition} endpoint: {endpoint}");
                    Console.WriteLine($"req: {JsonConvert.SerializeObject(item)}");
                    var responseApi = await this._apiService.RequestMethodPost("SyncShipmentEndpoint1", "Shipment", JsonConvert.SerializeObject(item));
                    Console.WriteLine($"res: {JsonConvert.SerializeObject(responseApi)}");
                    bool isSent = (responseApi.statusCode == 200 && !string.IsNullOrEmpty(responseApi.responseMessage)) ? true : false;
                    tmpSyncShipmentInfos.Add(new()
                    {
                        sync_id = item.sync_id,
                        processed = isSent,
                        processed_date = DateTime.Now,
                    });
                }

                if (tmpSyncShipmentInfos.Count > 0)
                {
                    await this._syncShipmentInfoRepository.UpdateSyncShipmentInfo(tmpSyncShipmentInfos);
                }
            }
        }

    }
}
