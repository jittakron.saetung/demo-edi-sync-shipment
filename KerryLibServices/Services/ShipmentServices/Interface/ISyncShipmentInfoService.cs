﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Services.ShipmentServices.Interface
{
    public interface ISyncShipmentInfoService
    {
        Task SyncShipmentInfo(int partition, int mod);
    }
}
