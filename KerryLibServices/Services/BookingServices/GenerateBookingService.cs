﻿using KerryLibServices.Models.Booking;
using KerryLibServices.Repository.Interface;
using KerryLibServices.Services.BookingServices.Interface;
using KerryLibServices.Services.EdiServices.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static KerryLibServices.Models.Booking.IsHolidayDTO;

namespace KerryLibServices.Services.BookingServices
{
    public class GenerateBookingService : IGenerateBookingService
    {
        private readonly IEdiService _ediService;
        private readonly IBookingRepository _bookingRepository;
        public GenerateBookingService(IEdiService ediService, IBookingRepository bookingRepository)
        {
            this._ediService = ediService;
            this._bookingRepository = bookingRepository;
        }

        public async Task GenerateBookingInfo(DateTime processDate)
        {
            List<EdiCustomerAutoBooking> ediCustomerAutoBookings = await this.GetEdiCustomerAutoBookings(processDate);
            if (ediCustomerAutoBookings.Count > 0)
            {
                #region RequestIsHoliday
                RequestIsHoliday requestIsHoliday = new RequestIsHoliday() { date = processDate.ToString("yyyyMMdd") };
                var requestEdiService = await _ediService.RequestIsHoliday(requestIsHoliday);
                if (requestEdiService.statusCode == 200 && !string.IsNullOrEmpty(requestEdiService.responseMessage))
                {
                    ResponseIsHoliday resIsHoliday = JsonConvert.DeserializeObject<ResponseIsHoliday>(requestEdiService.responseMessage);
                    if (resIsHoliday.isholiday.GetValueOrDefault(false))
                    {
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Holiday == "Y").ToList();
                    }
                }
                #endregion

                List<EdiBookingType> ediBookingTypes = await this._bookingRepository.GetEdiBookingType();
                List<long> listExportID = new List<long>();
                foreach (var item in ediCustomerAutoBookings)
                {
                    string bookingType = ediBookingTypes.FirstOrDefault(x => x.ID == item.BookingTypeID).DescriptionEN;
                    List<EdiBookingInfo> ediBookingInfos = await this._bookingRepository.GetBookingInfoEDIShipment(30, item.PayerID, item.CutoffTime.ToString());
                    if (ediBookingInfos.Count > 0)
                    {
                        int takeInsert = 500;
                        await this._bookingRepository.DeleteSyncFlagBookingExport(item.PayerID);

                        for (int i = 0; i < ediBookingInfos.Count; i += takeInsert)
                        {
                            var insertList = ediBookingInfos.Skip(i).Take(takeInsert).ToList();
                            listExportID.AddRange(await this._bookingRepository.SaveBookingExport(insertList, bookingType, processDate));
                        }
                    }

                    item.ModifiedDate = processDate;
                    item.ModifiedBy = "GenerateBookingInfoToKESJob";
                }

                await this._bookingRepository.UpdateCustGenerateBooking(ediCustomerAutoBookings);
                if (listExportID.Count > 0)
                {
                    await this._bookingRepository.UpdateSyncFlagReadyToExport(listExportID);
                }
            }
            else
            {
                //หมายเหตุ: ทำวันละ 1 ครั้ง
                //ไม่มี job ให้ทำหรือทำไปแล้ว
            }
        }

        private async Task<List<EdiCustomerAutoBooking>> GetEdiCustomerAutoBookings(DateTime processDate)
        {
            List<EdiCustomerAutoBooking> ediCustomerAutoBookings = await _bookingRepository.GetEdiCustomerAutoBooking();
            if (ediCustomerAutoBookings.Count > 0)
            {
                ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => processDate.TimeOfDay >= x.CutoffTime).ToList();
                switch (processDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Monday == "Y").ToList();
                        break;
                    case DayOfWeek.Tuesday:
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Tuesday == "Y").ToList();
                        break;
                    case DayOfWeek.Wednesday:
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Wednesday == "Y").ToList();
                        break;
                    case DayOfWeek.Thursday:
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Thursday == "Y").ToList();
                        break;
                    case DayOfWeek.Friday:
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Friday == "Y").ToList();
                        break;
                    case DayOfWeek.Saturday:
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Saturday == "Y").ToList();
                        break;
                    case DayOfWeek.Sunday:
                        ediCustomerAutoBookings = ediCustomerAutoBookings.Where(x => x.Sunday == "Y").ToList();
                        break;
                }
            }

            return ediCustomerAutoBookings;
        }
    }
}
