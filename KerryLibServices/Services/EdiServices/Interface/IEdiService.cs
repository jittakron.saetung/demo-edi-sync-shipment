﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static KerryLibServices.Models.Booking.IsHolidayDTO;

namespace KerryLibServices.Services.EdiServices.Interface
{
    public interface IEdiService
    {
        Task<(int statusCode, string responseMessage)> RequestIsHoliday(RequestIsHoliday requestIsHoliday);
    }
}
