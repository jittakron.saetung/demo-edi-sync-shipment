﻿using KerryLibServices.Services.EdiServices.Interface;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static KerryLibServices.Models.Booking.IsHolidayDTO;
using static System.Net.Mime.MediaTypeNames;

namespace KerryLibServices.Services.EdiServices
{
    public class EdiService : IEdiService
    {
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        public EdiService(IConfiguration configuration, HttpClient httpClient)
        {
            this._configuration = configuration;
            this._httpClient = httpClient;
        }

        public async Task<(int statusCode, string responseMessage)> RequestIsHoliday(RequestIsHoliday requestIsHoliday)
        {
            string requestRef = string.Empty;
            StringContent dataContent = new(JsonConvert.SerializeObject(requestIsHoliday), Encoding.UTF8, Application.Json);

            try
            {
                string urlEndpoint = _configuration.GetSection("EDIConfigs").GetChildren().FirstOrDefault(config => config.Key == "EDIEndpoint").Value;
                string methodName = "CheckIsHoliday";
                using var task = await _httpClient.PostAsync(Path.Combine(urlEndpoint + methodName, requestRef), dataContent);

                int statusCode = (int)task.StatusCode;
                string resultMessage = await task.Content.ReadAsStringAsync();
                return (statusCode, resultMessage);
            }
            catch (Exception)
            {
                return (500, "Internal Error");
            }
        }

    }
}
