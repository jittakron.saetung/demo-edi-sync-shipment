﻿using KerryLibServices.Models.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Repository.Interface
{
    public interface IBookingRepository
    {
        Task<List<EdiCustomerAutoBooking>> GetEdiCustomerAutoBooking();
        Task<List<EdiBookingType>> GetEdiBookingType();
        Task<List<EdiBookingInfo>> GetBookingInfoEDIShipment(int backward, string payerId, string cutoffTime);
        Task DeleteSyncFlagBookingExport(string customerCode);
        Task<List<long>> SaveBookingExport(List<EdiBookingInfo> listBookExport, string bookingType, DateTime bookingDate);
        Task UpdateCustGenerateBooking(List<EdiCustomerAutoBooking> ediCustomerAutoBookings);
        Task UpdateSyncFlagReadyToExport(List<long> listExportID);
    }
}
