﻿using KerryLibServices.Models.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Repository.Interface
{
    public interface ITestRepository
    {
        Task<List<EdiCustomer>> GetEdiCustomer();
    }
}
