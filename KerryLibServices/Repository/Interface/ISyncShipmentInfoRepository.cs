﻿using KerryLibServices.Models.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Repository.Interface
{
    public interface ISyncShipmentInfoRepository
    {
        Task<List<EdiShipmentInfo>> GetEdiShipmentInfoSyncPending(int partition, int mod);
        Task UpdateSyncShipmentInfo(List<TmpSyncShipmentInfo> tmpSyncShipmentInfo);
    }
}
