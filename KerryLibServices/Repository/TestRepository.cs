﻿using KerryLibDataAccess.DataAccess.Interface;
using KerryLibServices.Models.Test;
using KerryLibServices.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Repository
{
    public class TestRepository : ITestRepository
    {
        private readonly ISqlDataAccess _dataAccess;
        public TestRepository(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public async Task<List<EdiCustomer>> GetEdiCustomer()
        {
            var sql = @"SELECT cust_id as CustID
	                          ,cust_name as CustName
	                          ,api_key as ApiKey
	                          ,[user_name] as [Username]
	                          ,pwd as [Password]
	                          ,master_account_id as MasterAccountID
	                          ,payerid as PayerID
	                          ,ISNULL(is_dropoff,0) as IsDropoff 
                        FROM tb_edi_customer WITH(NOLOCK)";

            IEnumerable<EdiCustomer> data = await _dataAccess.LoadData<EdiCustomer, dynamic>(sql, new { }, "KE_EDI");
            return data.ToList();
        }
    }
}
