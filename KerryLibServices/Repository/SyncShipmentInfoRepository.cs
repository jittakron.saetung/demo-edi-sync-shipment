﻿using KerryLibDataAccess.DataAccess.Interface;
using KerryLibServices.Models.Shipment;
using KerryLibServices.Repository.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Repository
{
    public class SyncShipmentInfoRepository : ISyncShipmentInfoRepository
    {
        private readonly ISqlDataAccess _dataAccess;
        private readonly IConfiguration _configuration;
        public SyncShipmentInfoRepository(ISqlDataAccess dataAccess, IConfiguration configuration)
        {
            this._dataAccess = dataAccess;
            this._configuration = configuration;
        }

        public async Task<List<EdiShipmentInfo>> GetEdiShipmentInfoSyncPending(int partition, int mod)
        {
            var sql = $@"SELECT TOP(100) e.*,t.sync_id 
                        FROM tb_edi_shipment as e with(nolock)
                        INNER JOIN tb_tmp_sync_aws as t with(nolock)
	                        ON e.consignment_no = t.consignment_no
                        WHERE t.processed IS NULL
                        AND t.sync_type = 'SHIPMENT_INFO'
                        AND sync_id % {mod} = {partition}
                        ORDER BY t.sync_id";

            IEnumerable<EdiShipmentInfo> data = await _dataAccess.LoadData<EdiShipmentInfo, dynamic>(sql, new { }, "KE_EDI");
            return data.ToList();
        }

        public async Task UpdateSyncShipmentInfo(List<TmpSyncShipmentInfo> tmpSyncShipmentInfo)
        {
            var formatUpdate = "SELECT {0} as sync_id, '{1}' as processed, '{2}' as processed_date";
            var listUpdate = new List<string>();

            tmpSyncShipmentInfo.ForEach(i =>
            {
                object[] parameters = new object[]
                {
                    i.sync_id,
                    i.processed,
                    i.processed_date.ToString("yyyy-MM-dd HH:mm:ss")
                };
                listUpdate.Add(string.Format(formatUpdate, parameters));
            });

            int retry = 0;
            bool updateSuccess = false;
            var sql = $@"UPDATE X SET
	                              processed = T.processed
	                            , processed_date = T.processed_date
                            FROM tb_tmp_sync_aws x (NOLOCK)
                            {string.Format("INNER JOIN ({0}) T ", string.Join(" UNION ALL ", listUpdate))}
                            ON X.sync_id = T.sync_id";
            do
            {
                int output = await _dataAccess.SaveDataExecuteAsync<int, dynamic>(sql, new { }, "KE_EDI");
                updateSuccess = output > 0;
                if (!updateSuccess)
                {
                    retry++;
                    Thread.Sleep(1000 * 2);
                }

            } while (updateSuccess == false && retry < 3);
        }

        //public async Task UpdateSyncShipmentInfo(List<TmpSyncShipmentInfo> tmpSyncShipmentInfo)
        //{
        //    List<string> listUpdate = new();
        //    int custTake = 20;
        //    var formatUpdate = "SELECT {0} as sync_id, '{1}' as processed, '{2}' as processed_date";
        //    var n = Math.Ceiling((double)tmpSyncShipmentInfo.Count / custTake);
        //    for (int i = 0; i < n; i++)
        //    {
        //        var cutUpdate = tmpSyncShipmentInfo.Skip(i * custTake).Take(custTake).ToList();
        //        cutUpdate.ForEach(item =>
        //        {
        //            object[] parameters = new object[]
        //            {
        //                item.sync_id,
        //                item.processed,
        //                item.processed_date.ToString("yyyy-MM-dd HH:mm:ss")
        //            };
        //            listUpdate.Add(string.Format(formatUpdate, parameters));
        //        });

        //        int retry = 0;
        //        bool updateSuccess = false;
        //        var sql = $@"UPDATE X SET
        //                       processed = T.processed
        //                     , processed_date = T.processed_date
        //                    FROM tb_tmp_sync_aws x (NOLOCK)
        //                    {string.Format("INNER JOIN ({0}) T ", string.Join(" UNION ALL ", listUpdate))}
        //                    ON X.sync_id = T.sync_id";
        //        do
        //        {
        //            int output = await _dataAccess.SaveDataExecuteAsync<int, dynamic>(sql, new { }, "KE_EDI");
        //            Console.WriteLine(sql);
        //            Console.WriteLine(output);
        //            updateSuccess = output > 0;
        //            if (!updateSuccess)
        //            {
        //                retry++;
        //                Thread.Sleep(1000 * 2);
        //            }

        //        } while (updateSuccess == false && retry < 3);
        //    }
        //}
    }
}
