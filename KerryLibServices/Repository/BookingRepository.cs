﻿using KerryLibDataAccess.DataAccess.Interface;
using KerryLibServices.Models.Booking;
using KerryLibServices.Models.Test;
using KerryLibServices.Repository.Interface;
using KerryLibServices.Utilities.Interface;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KerryLibServices.Repository
{
    public class BookingRepository : IBookingRepository
    {
        private readonly ISqlDataAccess _dataAccess;
        private readonly IFunctionHelper _functionHelper;
        public BookingRepository(ISqlDataAccess dataAccess, IFunctionHelper functionHelper)
        {
            this._dataAccess = dataAccess;
            this._functionHelper = functionHelper;
        }

        public async Task<List<EdiCustomerAutoBooking>> GetEdiCustomerAutoBooking()
        {
            var sql = @"SELECT   ID
		                        ,payerid as PayerID
		                        ,ISNULL(backward,5) as Backward
		                        ,active as Active
		                        ,cutoff_time as CutoffTime
		                        ,ISNULL(booking_type_id,7) as BookingTypeID
		                        ,sunday as Sunday
		                        ,monday as Monday
		                        ,tuesday as Tuesday
		                        ,wednesday as Wednesday
		                        ,thursday as Thursday
		                        ,friday as Friday
		                        ,saturday as Saturday
		                        ,holiday as Holiday
                        FROM tb_edi_customer_booking with(nolock)
                        WHERE active = 'Y' and isnull(modified_date, CAST(GETDATE() - 1 as DATE)) < CAST(GETDATE() as DATE)";

            IEnumerable<EdiCustomerAutoBooking> data = await _dataAccess.LoadData<EdiCustomerAutoBooking, dynamic>(sql, new { }, "KE_EDI");
            return data.ToList();
        }

        public async Task<List<EdiBookingType>> GetEdiBookingType()
        {
            var sql = @"SELECT   ID
		                        ,[Type] as BookingType
		                        ,[Value] as [Value]
		                        ,Active_Status as Active
		                        ,[Description] as DescriptionEN
		                        ,Description_TH as DescriptionTH
                        FROM tb_list_of_value(NOLOCK) WHERE Type = 'BOOKING_TYPE' AND Active_Status = 'Y'";

            IEnumerable<EdiBookingType> data = await _dataAccess.LoadData<EdiBookingType, dynamic>(sql, new { }, "KE_EDI");
            return data.ToList();
        }

        public async Task<List<EdiBookingInfo>> GetBookingInfoEDIShipment(int backward, string payerId, string cutoffTime)
        {
            string spName = (payerId != "LALEXDOF") ? "sp_GetBookingInfoEDIShipment" : "sp_GetBookingInfoEDIShipmentFromOrigin";
            var parameters = new
            {
                backward = backward,
                cust_id = payerId,
                cutoff = cutoffTime
            };

            IEnumerable<EdiBookingInfo> data = await _dataAccess.ExecuteStoredProcedure<EdiBookingInfo, dynamic>(spName, parameters, "KE_EDI");
            return data.ToList();
        }

        public async Task DeleteSyncFlagBookingExport(string customerCode)
        {
            var parameters = new { customerCode = customerCode };
            var sql = @"DELETE FROM tb_tmp_export_booking WHERE customer_code = @customerCode AND Sync_Flag = 'W'";
            await _dataAccess.SaveData(sql, parameters, "KE_EDI");
        }

        public async Task<List<long>> SaveBookingExport(List<EdiBookingInfo> listBookExport, string bookingType, DateTime bookingDate)
        {
            var sql = "INSERT INTO tb_tmp_export_booking (booking_datetime, booking_type, customer_code, address, address2, sender_code, sender_name, contract_person, email, mobile_no1, mobile_no2, phone_no, postcode, remark, tot_con, tot_pkg, created_date, created_by, Sync_Flag) OUTPUT INSERTED.ID ";
            foreach (var item in listBookExport)
            {
                object[] parameters = new object[] 
                {
                    bookingDate.ToString("yyyyMMdd HH:mm:ss", new CultureInfo("en-US")),
                    bookingType,
                    item.cust_id,
                    item.address1,
                    item.address2,
                    item.merchant_id,
                    item.merchant_name,
                    item.contact_person,
                    item.email,
                    this._functionHelper.ReplaceNumberPhone(item.mobile1),
                    this._functionHelper.ReplaceNumberPhone(item.mobile2),
                    this._functionHelper.ReplaceNumberPhone(item.telephone),
                    item.zipcode,
                    item.cust_remark,
                    item.total_con,
                    item.total_pkg,
                    "GenerateBookingInfoToKESJob",
                    "W"
                };

                sql += string.Format("SELECT '{0}', '{1}', '{2}', N'{3}', N'{4}', '{5}', N'{6}', N'{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', {14}, {15}, GETDATE(), '{16}', '{17}' ", parameters);
                if (item != listBookExport.Last()) 
                    sql += "UNION ALL";
            }

            IEnumerable<long> data = await _dataAccess.LoadData<long, dynamic>(sql, new { }, "KE_EDI");
            return data.ToList();
        }

        public async Task UpdateCustGenerateBooking(List<EdiCustomerAutoBooking> ediCustomerAutoBookings)
        {
            var formatUpdate = "SELECT {0} as ID, '{1}' as modified_date, '{2}' as modified_by";
            var listUpdate = new List<string>();
            foreach (var item in ediCustomerAutoBookings)
            {
                object[] parameters = new object[]
                {
                    item.ID,
                    item.ModifiedDate.GetValueOrDefault().ToString("yyyyMMdd HH:mm:ss"),
                    item.ModifiedBy
                };
                listUpdate.Add(string.Format(formatUpdate, parameters));
            }
            int retry = 0;
            bool updateSuccess = false;
            var sql = $@"UPDATE X SET
	                            modified_date = T.modified_date
	                            , modified_by = T.modified_by
                            FROM tb_edi_customer_booking x (NOLOCK)
                            {string.Format("INNER JOIN ({0}) T ", string.Join(" UNION ALL ", listUpdate))}
                            ON X.ID = T.ID";
            do
            {
                int output = await _dataAccess.SaveDataExecuteAsync<int, dynamic>(sql, new { }, "KE_EDI");
                updateSuccess = output > 0;
                if (!updateSuccess)
                {
                    retry++;
                    Thread.Sleep(1000 * 2);
                }

            } while (updateSuccess == false && retry < 3);
        }

        public async Task UpdateSyncFlagReadyToExport(List<long> listExportID)
        {
            int take = 1000;
            for (int i = 0; i < listExportID.Count; i+= take)
            {
                var updateList = listExportID.Skip(i).Take(take).ToList();
                int retry = 0;
                bool updateSuccess = false;
                var formatUpdate = "SELECT {0} as ID";
                List<string> listUpdate = new List<string>();
                do
                {
                    foreach (var item in updateList)
                    {
                        listUpdate.Add(string.Format(formatUpdate, item));
                    }

                    var sql = $@"UPDATE X SET
	                                Sync_Flag = 'N'
	                                , modified_date = GETDATE()
	                                , modified_by = 'GenerateBookingInfoToKESJob'
                                FROM tb_tmp_export_booking x (NOLOCK)
                                {string.Format("INNER JOIN ({0}) T ", string.Join(" UNION ALL ", listUpdate))}
                                ON X.ID = T.ID";

                    int output = await _dataAccess.SaveDataExecuteAsync<int, dynamic>(sql, new { }, "KE_EDI");
                    updateSuccess = output > 0;
                    if (!updateSuccess)
                    {
                        retry++;
                        Thread.Sleep(1000 * 2);
                    }

                } while (updateSuccess == false && retry < 3);
            }
        }
    }
}
