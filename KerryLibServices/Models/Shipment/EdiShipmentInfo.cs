﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Models.Shipment
{
    public class TmpSyncShipmentInfo
    {
        public int sync_id { get; set; }
        public bool processed { get; set; }
        public DateTime processed_date { get; set; }
    }
    public class EdiShipmentInfo
    {
        public int sync_id { get; set; }
        public string cust_id { get; set; }
        public string consignment_no { get; set; }
        public string sender_name { get; set; }
        public string sender_address { get; set; }
        public string sender_village { get; set; }
        public string sender_soi { get; set; }
        public string sender_road { get; set; }
        public string sender_subdistrict { get; set; }
        public string sender_district { get; set; }
        public string sender_province { get; set; }
        public string sender_zipcode { get; set; }
        public string sender_mobile1 { get; set; }
        public string sender_mobile2 { get; set; }
        public string sender_telephone { get; set; }
        public string sender_email { get; set; }
        public string sender_contactperson { get; set; }
        public string recipient_name { get; set; }
        public string recipient_address { get; set; }
        public string recipient_village { get; set; }
        public string recipient_soi { get; set; }
        public string recipient_road { get; set; }
        public string recipient_subdistrict { get; set; }
        public string recipient_district { get; set; }
        public string recipient_province { get; set; }
        public string recipient_zipcode { get; set; }
        public string recipient_mobile1 { get; set; }
        public string recipient_mobile2 { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_email { get; set; }
        public string recipient_contactperson { get; set; }
        public string special_note { get; set; }
        public string service_code { get; set; }
        public double? cod_amount { get; set; }
        public string cod_type { get; set; }
        public int tot_pkg { get; set; }
        public DateTime created_date { get; set; }
        public double? declare_value { get; set; }
        public string ref_no { get; set; }
        public string action_code { get; set; }
        public string cust_remark { get; set; }
        public string ref_no2 { get; set; }
        public string original_consignment_no { get; set; }
        public int shipment_type { get; set; }
        public int? extra_service { get; set; }
        public string invr { get; set; }
        public double? goods_value { get; set; }
        public string merchant_id { get; set; }
        public int? package_type { get; set; }
        public string cod_account_id { get; set; }
        public double? del_fee { get; set; }
        public double? pup_fee { get; set; }
        public double? cod_fee { get; set; }
        public int? pickup_type { get; set; }
        public string request_pickup_date { get; set; }
        public string request_pickup_time { get; set; }
    }
}
