﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Models.Test
{
    public class EdiCustomer
    {
        public string CustId { get; set; }
        public string CustName { get; set; }
        public string ApiKey { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string MasterAccountID { get; set; }
        public string PayerID { get; set; }
        public bool IsDropoff { get; set; }
    }
}
