﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Models.Booking
{
    public class EdiBookingInfo
    {
        public string cust_id { get; set; }
        public string merchant_id { get; set; }
        public string merchant_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string zipcode { get; set; }
        public string telephone { get; set; }
        public string contact_person { get; set; }
        public string email { get; set; }
        public string mobile1 { get; set; }
        public string mobile2 { get; set; }
        public int total_con { get; set; }
        public int total_pkg { get; set; }
        public string cust_remark { get; set; }
    }
}
