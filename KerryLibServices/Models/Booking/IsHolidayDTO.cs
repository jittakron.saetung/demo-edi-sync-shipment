﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Models.Booking
{
    public class IsHolidayDTO
    {
        public class RequestIsHoliday
        {
            public string date { get; set; }
        }

        public class ResponseIsHoliday
        {
            public string result_code { get; set; }
            public string result_desc { get; set; }
            public bool? isholiday { get; set; }
        }

    }
}
