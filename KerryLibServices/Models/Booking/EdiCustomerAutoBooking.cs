﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Models.Booking
{
    public class EdiCustomerAutoBooking
    {
        public int ID { get; set; }
        public string PayerID { get; set; }
        public int Backward { get; set; }
        public string Active { get; set; }
        public TimeSpan CutoffTime { get; set; }
        public int BookingTypeID { get; set; }
        public string Sunday { get;set; }
        public string Monday { get; set; }
        public string Tuesday { get;set;}
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Holiday { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
