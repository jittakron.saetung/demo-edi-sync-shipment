﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Models.Booking
{
    public class EdiBookingType
    {
        public int ID { get; set; }
        public string BookingType { get; set; }
        public string Value { get; set; }
        public string Active { get;set; }
        public string DescriptionEN { get; set; }
        public string DescriptionTH { get; set; }
    }
}
