﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Utilities.Interface
{
    public interface IFunctionHelper
    {
        string GetVersionLib();
        string ReplaceNumberPhone(string str);
    }
}
