﻿using KerryLibServices.Utilities.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace KerryLibServices.Utilities
{
    public class FunctionHelper : IFunctionHelper
    {
        public string GetVersionLib()
        {
            Assembly assembly =Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string versionLib = "V" + fvi.FileVersion;

            return versionLib;
        }

        public string ReplaceEnter(string obj)
        {
            return string.Concat(obj).Replace(@"\r\n", string.Empty);
        }

        public string ReplaceNumberPhone(string str)
        {
            string number = new string(string.Concat(ReplaceEnter(str)).ToCharArray().Where(c => Char.IsDigit(c)).ToArray());

            if (number.StartsWith("+66"))
            {
                number = number.Replace("+66", "0");
            }
            else if (number.StartsWith("66"))
            {
                number = string.Concat("0", number.Substring(2));
            }

            if (number.StartsWith("00"))
            {
                number = string.Concat("0", number.Substring(2));
            }

            return number;
        }
    }
}
