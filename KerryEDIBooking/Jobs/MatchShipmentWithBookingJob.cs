﻿using KerryLibServices.Services.BookingServices.Interface;
using Quartz;

namespace KerryEDIBooking.Jobs
{
    [DisallowConcurrentExecution]
    public class MatchShipmentWithBookingJob : IJob
    {
        private readonly IMatchShipmentService _matchShipmentService;
        public MatchShipmentWithBookingJob(IMatchShipmentService matchShipmentService)
        {
            this._matchShipmentService = matchShipmentService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            Console.WriteLine("MatchShipmentWithBookingJob: => " + DateTime.Now.ToLongTimeString());
        }
    }
}
