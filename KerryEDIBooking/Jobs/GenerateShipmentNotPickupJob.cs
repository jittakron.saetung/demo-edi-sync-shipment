﻿using KerryLibServices.Services.BookingServices.Interface;
using Quartz;

namespace KerryEDIBooking.Jobs
{
    [DisallowConcurrentExecution]
    public class GenerateShipmentNotPickupJob : IJob
    {
        private readonly IGenerateShipmentService _generateShipmentService;
        public GenerateShipmentNotPickupJob(IGenerateShipmentService generateShipmentService)
        {
            this._generateShipmentService = generateShipmentService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            Console.WriteLine("GenerateShipmentNotPickupJob: => " + DateTime.Now.ToLongTimeString());
        }
    }
}
