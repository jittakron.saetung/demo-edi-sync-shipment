﻿using KerryLibServices.Services.BookingServices.Interface;
using Quartz;
using System.Collections.Generic;
using System.Diagnostics;

namespace KerryEDIBooking.Jobs
{
    [DisallowConcurrentExecution]
    public class GenerateBookingInfoToKESJob : IJob
    {
        private readonly IGenerateBookingService _generateBookingService;
        public GenerateBookingInfoToKESJob(IGenerateBookingService generateBookingService)
        {
            this._generateBookingService = generateBookingService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            Console.WriteLine("GenerateBookingInfoToKESJob: => " + DateTime.Now.ToLongTimeString());
            await _generateBookingService.GenerateBookingInfo(DateTime.Now);

            int sleepMinTime = 10;
            Thread.Sleep(1000 * 60 * sleepMinTime);
        }
    }
}
