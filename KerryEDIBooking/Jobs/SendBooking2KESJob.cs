﻿using KerryLibServices.Services.BookingServices.Interface;
using Quartz;

namespace KerryEDIBooking.Jobs
{
    [DisallowConcurrentExecution]
    public class SendBooking2KESJob : IJob
    {
        private readonly ISendBookingToKesService _sendBookingToKesService;
        public SendBooking2KESJob(ISendBookingToKesService sendBookingToKesService)
        {
            this._sendBookingToKesService = sendBookingToKesService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            Console.WriteLine("SendBooking2KESJob: => " + DateTime.Now.ToLongTimeString());
        }
    }
}
