﻿using Quartz;

namespace KerryEDIBooking.Config
{
    public static class JobConfiguratorExtensions
    {
        public static void AddJobAndTrigger<T>(this IServiceCollectionQuartzConfigurator quartz, IConfiguration config, string group) where T : IJob
        {
            string jobName = typeof(T).Name;

            var configKey = $"Cron:{group}-Group:{jobName}";
            var cronSchedule = config[configKey];

            if (string.IsNullOrEmpty(cronSchedule))
            {
                cronSchedule = "/1 * * ? * *";
            }

            var jobKey = new JobKey(jobName, group);
            quartz.AddJob<T>(opt => opt.WithIdentity(jobKey));

            quartz.AddTrigger(opt => opt
                .ForJob(jobKey)
                .WithIdentity(jobName + "-trigger")
                .WithCronSchedule(cronSchedule));
        }
    }
}
