﻿using KerryEDIBooking.Jobs;
using KerryLibDataAccess.DataAccess.Interface;
using KerryLibDataAccess.DataAccess;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Quartz;
using KerryLibServices.Utilities;
using KerryLibServices.Utilities.Interface;
using KerryLibServices.Repository.Interface;
using KerryLibServices.Repository;
using System.Net.Http.Headers;
using KerryLibServices.Services.BookingServices.Interface;
using KerryLibServices.Services.BookingServices;
using KerryLibServices.Services.EdiServices;
using KerryLibServices.Services.EdiServices.Interface;

namespace KerryEDIBooking.Config
{
    public static class ApplicationServiceConfig
    {
        public static IServiceCollection AddApplicationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<IISServerOptions>(o => { o.AllowSynchronousIO = true; });
            services.Configure<KestrelServerOptions>(o => { o.AllowSynchronousIO = true; });
            services.AddHealthChecks();

            services.AddQuartzService(configuration);
            services.AddQuartzServer(options => { options.WaitForJobsToComplete = true; });

            services.AddScoped<ISqlDataAccess, SqlDataAccess>();
            services.AddScoped<IFunctionHelper, FunctionHelper>();
            services.AddScoped<ITestRepository, TestRepository>();
            services.AddScoped<IGenerateBookingService, GenerateBookingService>();
            services.AddScoped<ISendBookingToKesService, SendBookingToKesService>();
            services.AddScoped<IGenerateShipmentService, GenerateShipmentService>();
            services.AddScoped<IMatchShipmentService, MatchShipmentService>();
            services.AddScoped<IBookingRepository, BookingRepository>();

            services.AddHttpClient<IEdiService, EdiService>(c =>
            {
                c.BaseAddress = new Uri(configuration.GetValue<string>("EDIConfigs:EDIEndpoint"));
                c.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });
            return services;
        }

        private static IServiceCollection AddQuartzService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();

                q.AddJobAndTrigger<GenerateBookingInfoToKESJob>(configuration, "BOOKING");
                //q.AddJobAndTrigger<SendBooking2KESJob>(configuration, "BOOKING");
                //q.AddJobAndTrigger<MatchShipmentWithBookingJob>(configuration, "SHIPMENT");
                //q.AddJobAndTrigger<GenerateShipmentNotPickupJob>(configuration, "SHIPMENT");
            });
            return services;
        }
    }
}
