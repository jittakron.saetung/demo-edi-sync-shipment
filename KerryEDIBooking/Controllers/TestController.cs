﻿using KerryEDIBooking.Models;
using KerryLibServices.Repository.Interface;
using Microsoft.AspNetCore.Mvc;

namespace KerryEDIBooking.Controllers
{
    [ApiController]
    public class TestController : Controller
    {
        private readonly ITestRepository _testRepository;
        public TestController(ITestRepository testRepository)
        {
            this._testRepository = testRepository;
        }

        [HttpGet("test/edicustomer")]
        public async Task<IActionResult> EdiCustomer()
        {
            var ediCustomer = await _testRepository.GetEdiCustomer();
            return Ok(new ResponseModel<dynamic>(false, "000", "Success", ediCustomer));
        }
    }
}
