﻿using System.Reflection;

namespace KerryEDIBooking.Utilities
{
    public static class FunctionHelper
    {
        public static string GetVersionProgram(string fileName)
        {
            string buildTime = GetLinkerTimestampUtc(Assembly.GetExecutingAssembly()).ToString("yyyyMMdd-HHmm");
            string fileVersion = Directory.GetCurrentDirectory() + $@"/{fileName}.txt";
            string readFileVersion = File.ReadAllText(fileVersion);

            return "V" + readFileVersion + " Release[" + File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location).ToString("yyyyMMdd-HHmm") + "]";
        }

        public static DateTime GetLinkerTimestampUtc(Assembly assembly)
        {
            var location = assembly.Location;
            return GetLinkerTimestampUtc(location);
        }

        public static DateTime GetLinkerTimestampUtc(string filePath)
        {
            const int peHeaderOffset = 60;
            const int linkerTimestampOffset = 8;
            var bytes = new byte[2048];

            using (var file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                file.Read(bytes, 0, bytes.Length);
            }

            var headerPos = BitConverter.ToInt32(bytes, peHeaderOffset);
            var secondsSince = BitConverter.ToInt32(bytes, headerPos + linkerTimestampOffset);
            var dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return dt.AddSeconds(secondsSince);
        }
    }
}
