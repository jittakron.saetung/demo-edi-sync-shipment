﻿namespace KerryEDIBooking.Models
{
    public class ResponseModel<T>
    {
        public bool Error { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public ResponseDisplayModel Display { get; set; }
        public T Result { get; set; }

        public ResponseModel() { }

        public ResponseModel(bool IsError, string Code, string Desc, string DisplayTh, string DisplayEn)
        {
            Error = IsError;
            this.Code = Code;
            Description = Desc;
            Display = new ResponseDisplayModel()
            {
                Th = DisplayTh,
                En = DisplayEn
            };
        }

        public ResponseModel(bool IsError, string Code, string Desc)
        {
            Error = IsError;
            this.Code = Code;
            Description = Desc;
        }

        public ResponseModel(bool IsError, string Code, string Desc, T Result)
        {
            Error = IsError;
            this.Code = Code;
            Description = Desc;
            this.Result = Result;
        }

        public ResponseModel(bool IsError, string Code, string Desc, string DisplayTh, string DisplayEn, T Result)
        {
            Error = IsError;
            this.Code = Code;
            Description = Desc;
            Display = new ResponseDisplayModel()
            {
                Th = DisplayTh,
                En = DisplayEn
            };
            this.Result = Result;
        }
    }

    public class ResponseDisplayModel
    {
        public string Th { get; set; }
        public string En { get; set; }
    }

}
