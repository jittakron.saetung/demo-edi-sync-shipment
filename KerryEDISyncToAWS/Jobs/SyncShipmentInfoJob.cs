﻿using KerryLibServices.Services.ShipmentServices.Interface;
using Quartz;
using System.Diagnostics;

namespace KerryEDISyncToAWS.Jobs
{
    [DisallowConcurrentExecution]
    public class SyncShipmentInfoJob : IJob
    {
        private readonly ISyncShipmentInfoService _syncShipmentInfoService;
        public SyncShipmentInfoJob(ISyncShipmentInfoService syncShipmentInfoService)
        {
            this._syncShipmentInfoService = syncShipmentInfoService;
        }

        public Task Execute(IJobExecutionContext context)
        {
            List<Task> TaskList = new List<Task>();
            List<int> listId = Enumerable.Range(0, 3).ToList();

            listId.ForEach(i =>
            {
                var t1 = new Task<int>(() =>
                {
                    return ExecuteJob(i, listId.Count());
                });
                t1.Start();
                TaskList.Add(t1);
            });

            Task.WaitAll(TaskList.ToArray());
            if (TaskList.Sum(t => ((Task<int>)t).Result) == 0)
            {
                int sleep = 1;
                Thread.Sleep(1000 * 60 * sleep);
            }

            return Task.CompletedTask;
        }

        private int ExecuteJob(int partition, int mod)
        {
            //var timer = new Stopwatch();
            //timer.Start();
            //Console.WriteLine($"SyncShipmentToAWS {partition}: => " + DateTime.Now.ToLongTimeString());
            this._syncShipmentInfoService.SyncShipmentInfo(partition, mod);

            //timer.Stop();
            //TimeSpan timeTaken = timer.Elapsed;
            //Console.WriteLine($"Time taken {partition}: => {timeTaken.ToString(@"m\:ss\.fff")}");

            return 1;
        }
    }
}
