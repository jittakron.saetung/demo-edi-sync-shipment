﻿using KerryEDISyncToAWS.Jobs;
using KerryLibDataAccess.DataAccess;
using KerryLibDataAccess.DataAccess.Interface;
using KerryLibServices.Repository;
using KerryLibServices.Repository.Interface;
using KerryLibServices.Services.ApiServices;
using KerryLibServices.Services.ApiServices.Interface;
using KerryLibServices.Services.ShipmentServices;
using KerryLibServices.Services.ShipmentServices.Interface;
using KerryLibServices.Utilities;
using KerryLibServices.Utilities.Interface;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Quartz;
using System.Net.Http.Headers;

namespace KerryEDISyncToAWS.Config
{
    public static class ApplicationServiceConfig
    {
        public static IServiceCollection AddApplicationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<IISServerOptions>(o => { o.AllowSynchronousIO = true; });
            services.Configure<KestrelServerOptions>(o => { o.AllowSynchronousIO = true; });
            services.AddHealthChecks();

            services.AddQuartzService(configuration);
            services.AddQuartzServer(options => { options.WaitForJobsToComplete = true; });

            services.AddScoped<ISqlDataAccess, SqlDataAccess>();
            services.AddScoped<IFunctionHelper, FunctionHelper>();
            
            services.AddScoped<ISyncShipmentInfoService, SyncShipmentInfoService>();

            services.AddScoped<ITestRepository, TestRepository>();
            services.AddScoped<ISyncShipmentInfoRepository, SyncShipmentInfoRepository>();


            services.AddHttpClient<IApiService, ApiService>(c =>
            {
                c.BaseAddress = new Uri(configuration.GetValue<string>("APIConfigs:SyncShipmentEndpoint1"));
                c.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });
            return services;
        }

        private static IServiceCollection AddQuartzService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();

                q.AddJobAndTrigger<SyncShipmentInfoJob>(configuration, "SYNCSHIPMENT");
            });
            return services;
        }
    }
}
